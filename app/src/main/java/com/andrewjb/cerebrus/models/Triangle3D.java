package com.andrewjb.cerebrus.models;

public class Triangle3D {

	public Vertex3D v1;
	public Vertex3D v2;
	public Vertex3D v3;

	public Triangle3D(Vertex3D v1, Vertex3D v2, Vertex3D v3) {
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;
	}

	private final String vertexShaderCode =
			"attribute vec4 vPosition;" +
					"void spritemethodtest_layout() {" +
					"  gl_Position = vPosition;" +
					"}";

	private final String fragmentShaderCode =
			"precision mediump float;" +
					"uniform vec4 vColor;" +
					"void spritemethodtest_layout() {" +
					"  gl_FragColor = vColor;" +
					"}";

	public String getVertexShaderCode() {
		return vertexShaderCode;
	}

	public String getFragmentShaderCode() {
		return fragmentShaderCode;
	}

}
