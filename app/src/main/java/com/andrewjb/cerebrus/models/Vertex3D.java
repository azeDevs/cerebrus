package com.andrewjb.cerebrus.models;

public class Vertex3D {

	public float x;
	public float y;
	public float z;

	public Vertex3D(double x, double y, double z) {
		this.x = (float) x;
		this.y = (float) y;
		this.z = (float) z;
	}

	public Vertex3D(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

}
