package com.andrewjb.cerebrus.models;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.andrewjb.cerebrus.R;

public class Art {

	public static Bitmap drone;

	public static void loadAllBitmaps(Activity activity) {
		final AssetManager mgr = activity.getAssets();
		drone = load(activity, R.drawable.anim_drone_sw_walk);
	}

	private static Bitmap load(Context context, int filename) {
		Bitmap result = null;
		result = BitmapFactory.decodeResource(context.getResources(), filename);
		return result;
	}

}
