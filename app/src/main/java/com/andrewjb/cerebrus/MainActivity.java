package com.andrewjb.cerebrus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.andrewjb.cerebrus.gl20.GLES20Activity;
import com.andrewjb.cerebrus.models.Art;
import com.andrewjb.cerebrus.spritemethodtest.SpriteMethodTest;


public class MainActivity extends AppCompatActivity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Art.loadAllBitmaps(this);
	}

	public void startGLES20(View view) {
		Intent intent = new Intent(this, GLES20Activity.class);
		startActivity(intent);
	}

	public void startTest(View view) {
		Intent intent = new Intent(this, SpriteMethodTest.class);
		startActivity(intent);
	}

}
