package com.andrewjb.cerebrus.spritemethodtest;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.andrewjb.cerebrus.R;

/*
	SpriteMethodTest 1.0

        Author: Chris Pruett

        INTRODUCTION

        SpriteMethodTest is a simple application for comparing the relative speeds of
        various 2D drawing methods on Android.  The current version can test drawing
        with the CPU-based Canvas system, or via three different OpenGL ES rendering
        methods.

        The goal of this program is to provide an example of how to architect an
        efficient 2D rendering pipeline with the Android SDK, and to show the
        relative merits of each rendering method.  It is not an absolute benchmark and
        should not be used as such.  However, it should give you a good idea of how to
        go about making a fast 2D program.

        RENDERING METHODS

        There are four basic methods that SpriteMethodTest can test.

        1) Canvas.  This test draws sprites using Bitmap objects onto a Canvas surface.
        This is an entirely CPU-driven operation.  It is also the simplest to code.

        2) OpenGL ES flavors:

        2a) Basic Vert Quads.  This is a test of drawing sprites on a plane using
        basic vertex quads and orthographic projection.  This test requires only the
        baseline OpenGL ES 1.0, which is guaranteed to be supported on all Android
        devices.

        2b) Draw Texture Extension.  This test uses the GL_OES_draw_texture extension
        to draw sprites on the screen without any sort of projection or vertex buffers
        involved.  Note that this functionality is not guaranteed to be supported on
        all Android devices, and application authors who use it should check the
        contents of glGetString(GL_EXTENSIONS) before using it.  This extension is
        currently supported by the T-Mobile G1.

        2c) VBO Extension.  This test is fundamentally the same as 2a except that the
        vertex arrays describing the sprites are stored in vertex buffer objects,
        which means that they reside in the GPU's memory rather than in spritemethodtest_layout memory.
        As with 2b, this functionality is supported on the G1 but is not guaranteed
        to be supported on other devices; the extension string must be checked at
        runtime before you use it.

        INTERPRETING RESULTS

        After a test is run, a dialog containing some basic timing results will be
        displayed.  This dialog contains four numbers:

        - Frame Time.  This is the time (in ms and frames per second) that each frame
        took, on average, to draw from beginning to end.  This is the most valuable
        part of the result.

        - Draw Time.  This is the time that was spent, on average, making draw calls
        against the OpenGL ES or Canvas API.  Under Canvas this is mostly just the
        time that Bitmap.drawCanvas() required.  Under OpenGL ES, this time is the time
        it took to issue all GL commands for a single frame.

        - Page Flip Time.  This is a slightly misleading statistic.  It tracks the
        amount of time that the rendering thread spent waiting for the commands
        issued during Draw to actually appear on the screen.  Under the Canvas test
        this time is defined by the time spent in lockCanvas() and
        unlockCanvasAndPost().  Under Open GL ES this is the time spent in
        eglSwapBuffers().  This statistic is important because it shows how long
        your rendering thread may block while waiting for the previous frame to
        complete.  If this value is significant, you might want to move some
        non-rendering code into another thread.

        - Sim Time.  This is the time spent running the "simulation" step--the code
        that moves the robot sprites around on the screen.  If you uncheck the
        "animate" option, this time will always be zero.

        DESIGN NOTES

        All of the tests follow the same basic pattern: an activity to run the test
        starts up and spawns a separate thread to draw to the activity's surface.  This
        thread acquires a surface holder object and then draws into it as fast as
        it can.  This pattern is based on the SpriteText example from the API Demos
        sample, but it has been modified to work with both Canvas and OpenGL ES.

        FURTHER OPTIMIZATIONS

        This is not an absolutely optimal 2D drawing implementation, but it's fairly
        simple and produces good results. Other things that could be done to improve
        performance include better management of image and canvas bitmaps (or, in the
        OpenGL ES case, texture compression is an option on the G1).  The canvas is
        redrawn from scratch every frame, which probably isn't necessary; a
        dirty rectangle-based system might improve results.  There are other OpenGL ES
        extensions that might be applicable (such as point sprites).  The simulation
        code could easily be moved into another thread so that it can run while the
        rendering thread blocks on page flip.  And fixed point math could produce an
        edge over floating point math, especially when there are a large number of
        sprites on the screen.

        TAKEAWAY

        Here are some general observations about writing efficient 2D (and 3D) rendering
        systems on Android.

        - Canvas is super easy to use but has a low upper bound for performance.
        Though not tested in this example, rotation and scaling sprites using Canvas
        is probably very expensive.  Still, for a Tetris or color matching game, or
        even a top-down RPG that does not require 60fps, Canvas may be enough.

        - OpenGL ES is the way to go for 2D applications that need to maintain a high
        frame rate.  It's also the way to go if scaling or rotation are required, as
        these features are almost free.

        - OpenGL ES extensions, in particular the Draw Texture extension, can produce
        very good results on the G1.  However, not all devices are guaranteed to have
        these extensions, so your code should be built to handle differing rendering
        systems.  Hopefully this code shows how the same rendering pipeline and
        simulation code can be used with a variety of different rendering methods.

        - When drawing static vertex arrays (whether for 2D or 3D) on the G1, the
        vertex buffer object extension is a big win.  You should use it.

        - JNI call overhead is something to think about with OpenGL ES.  Each GL call
        will have to jump through JNI to get to the actual C++ implementation, and
        after a while that overhead may add up.  You might want to test drawing
        methods that are slower on their own but result in fewer total GL calls;
        for example, a tile-based game might be faster when rendered as a vertex grid
        (using the VBO extension) than as individual draw texture calls for each tile.
        This sample suggests that the draw texture extension is the fastest way to
        draw 2D sprites on the G1, but each draw call also requires at least one GL
        call, so in the case of a grid a vertex array might actually be faster.

        - One thing that this test does implicitly is avoid all allocation at runtime
        once the test has begun.  It invokes the GC before the test starts and then
        makes sure not to allocate memory explicitly (Java often allocates memory
        under the hood) so that the GC will not interfere with the test.  On the G1
        the GC takes between 100ms and 300ms to run, so for performance applications
        you *really* want to avoid allocating memory outside of safe pause states.
*/


/**
 * Main entry point for the SpriteMethodTest application.  This application
 * provides a simple interface for testing the relative speed of 2D rendering
 * systems available on Android, namely the Canvas system and OpenGL ES.  It
 * also serves as an example of how SurfaceHolders can be used to create an
 * efficient rendering thread for drawing.
 */
public class SpriteMethodTest extends Activity {

	private static final int ACTIVITY_TEST = 0;
	private static final int RESULTS_DIALOG = 0;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.spritemethodtest_layout);

		// Sets up a click listener for the Run Test button.
		Button button;
		button = (Button) findViewById(R.id.runTest);
		button.setOnClickListener(mRunTestListener);

		// Turns on one item by default in our radio groups--as it should be!
		RadioGroup group = (RadioGroup) findViewById(R.id.renderMethod);
		group.setOnCheckedChangeListener(mMethodChangedListener);
		group.check(R.id.methodCanvas);

		RadioGroup glSettings = (RadioGroup) findViewById(R.id.GLSettings);
		glSettings.check(R.id.settingVerts);

	}

	/**
	 * Passes preferences about the test via its intent.
	 */
	protected void initializeIntent(Intent i) {
		final CheckBox checkBox = (CheckBox) findViewById(R.id.animateSprites);
		final boolean animate = checkBox.isChecked();
		final EditText editText = (EditText) findViewById(R.id.spriteCount);
		final String spriteCountText = editText.getText().toString();
		final int stringCount = Integer.parseInt(spriteCountText);

		i.putExtra("animate", animate);
		i.putExtra("spriteCount", stringCount);
	}

	/**
	 * Responds to a click on the Run Test button by launching a new test
	 * activity.
	 */
	View.OnClickListener mRunTestListener = new View.OnClickListener() {
		public void onClick(View v) {
			RadioGroup group = (RadioGroup) findViewById(R.id.renderMethod);
			Intent i;
			if (group.getCheckedRadioButtonId() == R.id.methodCanvas) {
				i = new Intent(v.getContext(), CanvasTestActivity.class);
			} else {
				i = new Intent(v.getContext(), GLTestActivity.class);
				RadioGroup glSettings =
						(RadioGroup) findViewById(R.id.GLSettings);
				if (glSettings.getCheckedRadioButtonId() == R.id.settingVerts) {
					i.putExtra("useVerts", true);
				} else if (glSettings.getCheckedRadioButtonId()
						== R.id.settingVBO) {
					i.putExtra("useVerts", true);
					i.putExtra("useHardwareBuffers", true);
				}
			}
			initializeIntent(i);
			startActivityForResult(i, ACTIVITY_TEST);
		}
	};

	/**
	 * Enables or disables OpenGL ES-specific settings controls when the render
	 * method option changes.
	 */
	RadioGroup.OnCheckedChangeListener mMethodChangedListener
			= new RadioGroup.OnCheckedChangeListener() {
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			if (checkedId == R.id.methodCanvas) {
				findViewById(R.id.settingDrawTexture).setEnabled(false);
				findViewById(R.id.settingVerts).setEnabled(false);
				findViewById(R.id.settingVBO).setEnabled(false);
			} else {
				findViewById(R.id.settingDrawTexture).setEnabled(true);
				findViewById(R.id.settingVerts).setEnabled(true);
				findViewById(R.id.settingVBO).setEnabled(true);
			}
		}
	};

	/**
	 * Creates the test results dialog and fills in a dummy message.
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = null;
		if (id == RESULTS_DIALOG) {

			String dummy = "No results yet.";
			CharSequence sequence = dummy.subSequence(0, dummy.length() - 1);
			dialog = new AlertDialog.Builder(this)
					.setTitle("Profile Results")
					.setPositiveButton("Okay", null)
					.setMessage(sequence)
					.create();
		}
		return dialog;
	}

	/**
	 * Replaces the dummy message in the test results dialog with a string that
	 * describes the actual test results.
	 */
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == RESULTS_DIALOG) {
			// Extract final timing information from the profiler.
			final ProfileRecorder profiler = ProfileRecorder.sSingleton;
			final long frameTime =
					profiler.getAverageTime(ProfileRecorder.PROFILE_FRAME);
			final long frameMin =
					profiler.getMinTime(ProfileRecorder.PROFILE_FRAME);
			final long frameMax =
					profiler.getMaxTime(ProfileRecorder.PROFILE_FRAME);

			final long drawTime =
					profiler.getAverageTime(ProfileRecorder.PROFILE_DRAW);
			final long drawMin =
					profiler.getMinTime(ProfileRecorder.PROFILE_DRAW);
			final long drawMax =
					profiler.getMaxTime(ProfileRecorder.PROFILE_DRAW);

			final long flipTime =
					profiler.getAverageTime(ProfileRecorder.PROFILE_PAGE_FLIP);
			final long flipMin =
					profiler.getMinTime(ProfileRecorder.PROFILE_PAGE_FLIP);
			final long flipMax =
					profiler.getMaxTime(ProfileRecorder.PROFILE_PAGE_FLIP);

			final long simTime =
					profiler.getAverageTime(ProfileRecorder.PROFILE_SIM);
			final long simMin =
					profiler.getMinTime(ProfileRecorder.PROFILE_SIM);
			final long simMax =
					profiler.getMaxTime(ProfileRecorder.PROFILE_SIM);


			final float fps = frameTime > 0 ? 1000.0f / frameTime : 0.0f;

			String result = "Frame: " + frameTime + "ms (" + fps + " fps)\n"
					+ "\t\tMin: " + frameMin + "ms\t\tMax: " + frameMax + "\n"
					+ "Draw: " + drawTime + "ms\n"
					+ "\t\tMin: " + drawMin + "ms\t\tMax: " + drawMax + "\n"
					+ "Page Flip: " + flipTime + "ms\n"
					+ "\t\tMin: " + flipMin + "ms\t\tMax: " + flipMax + "\n"
					+ "Sim: " + simTime + "ms\n"
					+ "\t\tMin: " + simMin + "ms\t\tMax: " + simMax + "\n";
			CharSequence sequence = result.subSequence(0, result.length() - 1);
			AlertDialog alertDialog = (AlertDialog) dialog;
			alertDialog.setMessage(sequence);
		}
	}

	/**
	 * Shows the results dialog when the test activity closes.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
									Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		showDialog(RESULTS_DIALOG);

	}

}
