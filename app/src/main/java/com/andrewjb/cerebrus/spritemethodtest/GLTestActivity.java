package com.andrewjb.cerebrus.spritemethodtest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.andrewjb.cerebrus.R;

/**
 * Activity for testing OpenGL ES drawing speed.  This activity sets up sprites
 * and passes them off to an OpenGLSurfaceView for rendering and movement.
 */
public class GLTestActivity extends Activity {

	private final static int SPRITE_WIDTH = 64;
	private final static int SPRITE_HEIGHT = 64;


	private GLSurfaceView mGLSurfaceView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mGLSurfaceView = new GLSurfaceView(this);
		GLSimpleRenderer spriteRenderer = new GLSimpleRenderer(this);

		// Clear out any old profile results.
		ProfileRecorder.sSingleton.resetAll();

		final Intent callingIntent = getIntent();
		// Allocate our sprites and add them to an array.
		final int robotCount = callingIntent.getIntExtra("spriteCount", 10);
		final boolean animate = callingIntent.getBooleanExtra("animate", true);
		final boolean useVerts =
				callingIntent.getBooleanExtra("useVerts", false);
		final boolean useHardwareBuffers =
				callingIntent.getBooleanExtra("useHardwareBuffers", false);

		// Allocate space for the robot sprites + one test_background sprite.
		GLSprite[] spriteArray = new GLSprite[robotCount + 1];

		// We need to know the width and height of the display pretty soon,
		// so grab the information now.
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);

		GLSprite background = new GLSprite(R.drawable.test_background);
		BitmapDrawable backgroundImage = (BitmapDrawable) getResources().getDrawable(R.drawable.test_background);
		Bitmap backgoundBitmap = backgroundImage.getBitmap();
		background.width = backgoundBitmap.getWidth();
		background.height = backgoundBitmap.getHeight();
		if (useVerts) {
			// Setup the test_background grid.  This is just a quad.
			MeshGrid backgroundMeshGrid = new MeshGrid(2, 2, false);
			backgroundMeshGrid.set(0, 0, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, null);
			backgroundMeshGrid.set(1, 0, background.width, 0.0f, 0.0f, 1.0f, 1.0f, null);
			backgroundMeshGrid.set(0, 1, 0.0f, background.height, 0.0f, 0.0f, 0.0f, null);
			backgroundMeshGrid.set(1, 1, background.width, background.height, 0.0f,
					1.0f, 0.0f, null);
			background.setGrid(backgroundMeshGrid);
		}
		spriteArray[0] = background;


		MeshGrid spriteMeshGrid = null;
		if (useVerts) {
			// Setup a quad for the sprites to use.  All sprites will use the
			// same sprite grid intance.
			spriteMeshGrid = new MeshGrid(2, 2, false);
			spriteMeshGrid.set(0, 0, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, null);
			spriteMeshGrid.set(1, 0, SPRITE_WIDTH, 0.0f, 0.0f, 1.0f, 1.0f, null);
			spriteMeshGrid.set(0, 1, 0.0f, SPRITE_HEIGHT, 0.0f, 0.0f, 0.0f, null);
			spriteMeshGrid.set(1, 1, SPRITE_WIDTH, SPRITE_HEIGHT, 0.0f, 1.0f, 0.0f, null);
		}


		// This list of things to move. It points to the same content as the
		// sprite list except for the test_background.
		Renderable[] renderableArray = new Renderable[robotCount];
		final int robotBucketSize = robotCount / 3;
		for (int x = 0; x < robotCount; x++) {
			GLSprite robot;
			// Our robots come in three flavors.  Split them up accordingly.
			if (x < robotBucketSize) {
				robot = new GLSprite(R.drawable.test_skate1);
			} else if (x < robotBucketSize * 2) {
				robot = new GLSprite(R.drawable.test_skate2);
			} else {
				robot = new GLSprite(R.drawable.test_skate3);
			}

			robot.width = SPRITE_WIDTH;
			robot.height = SPRITE_HEIGHT;

			// Pick a random location for this sprite.
			robot.x = (float) (Math.random() * dm.widthPixels);
			robot.y = (float) (Math.random() * dm.heightPixels);

			// All sprites can reuse the same grid.  If we're running the
			// DrawTexture extension test, this is null.
			robot.setGrid(spriteMeshGrid);

			// Add this robot to the spriteArray so it gets drawn and to the
			// renderableArray so that it gets moved.
			spriteArray[x + 1] = robot;
			renderableArray[x] = robot;
		}


		// Now's a good time to run the GC.  Since we won't do any explicit
		// allocation during the test, the GC should stay dormant and not
		// influence our results.
		Runtime r = Runtime.getRuntime();
		r.gc();

		spriteRenderer.setSprites(spriteArray);
		spriteRenderer.setVertMode(useVerts, useHardwareBuffers);

		mGLSurfaceView.setRenderer(spriteRenderer);

		if (animate) {
			Mover simulationRuntime = new Mover();
			simulationRuntime.setRenderables(renderableArray);

			simulationRuntime.setViewSize(dm.widthPixels, dm.heightPixels);
			mGLSurfaceView.setEvent(simulationRuntime);
		}
		setContentView(mGLSurfaceView);
	}
}
